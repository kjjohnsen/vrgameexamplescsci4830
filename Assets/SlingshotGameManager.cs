﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingshotGameManager : MonoBehaviour
{

    public int level = 0;
    public List<GameObject> enemies;
    public float levelTime = 0;
    public AmmoSpawner ammoSpawner;
    // Start is called before the first frame update
    void Start()
    {
        gameReset();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void gameReset()
    {
        ammoSpawner.spawnAmmo = true;
    }
}
