﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slingshot: MonoBehaviour
    
{
    public Transform sphereCastPosition;
    public float radius;
    public float slingshotPower;
    SlingshotAmmunition loadedAmmo;
    // Start is called before the first frame update
     void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        
    }

    private void checkForNewAmmo()
    {
        Collider[] insideRadius = Physics.OverlapSphere(sphereCastPosition.position, radius);
        for (int i = 0; i < insideRadius.Length; i++)
        {
            Rigidbody rb = insideRadius[i].attachedRigidbody;
            if (rb == null) continue;

            SlingshotAmmunition ammo = rb.GetComponent<SlingshotAmmunition>();
            if (ammo == null) continue;

            if (!ammo.grabbed) continue;

            loadedAmmo = ammo;
            break;

        }
    }
    private void FixedUpdate()
    {


        if (loadedAmmo == null) { checkForNewAmmo(); }
        else
        {
            if (!loadedAmmo.grabbed)
            {
                Vector3 velocity = slingshotPower * (sphereCastPosition.position - loadedAmmo.transform.position);
                loadedAmmo.GetComponent<Rigidbody>().velocity = velocity;
                loadedAmmo = null;
            }
        }

        
        
    }
}
