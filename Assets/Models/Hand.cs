﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public Transform handAnchor;
    public Transform head;
    public Transform trackingSpace;
    public float speed;
    public OVRInput.Controller myHand;
    Grabbable grabbedObject;
    public Transform attachPoint;
    public Transform Laser;
    public GameObject arcPointPrefab;
    public float arcSpeed;
    public float snapRotateDelta;
    List<GameObject> arcPoints = new List<GameObject>();
    bool canSnapRotate = true;
    bool canDoWimTeleport = true;
    public bool useGoGo;
    // Start is called before the first frame update
    void Start()
    {
        attachPoint = Instantiate<GameObject>(new GameObject(), this.transform).GetComponent<Transform>();
    }

    void doTeleport(Vector3 targetPoint)
    {
        Vector3 footPos = head.position;
        footPos.y -= head.localPosition.y;
        Vector3 offset = targetPoint - footPos;

        ////bool trigger = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, myHand);
        //if (trigger)
       // {
            trackingSpace.Translate(offset, Space.World);
        //}
        
    }

    // Update is called once per frame
    void Update()
    {

        

        if (useGoGo)
        {
            Vector3 headToController = handAnchor.position - head.position;
            float d = headToController.magnitude;
            float s = 1;
            if (d > .5f)
            {
                s = 1 + 10 * (d - .5f);
            }
            transform.position = head.position + s * d * headToController.normalized;
        }
        //generate an arc, eminating from the laser forward and going outward at a certain velocity
        Vector3 arcVelocity = Laser.forward * arcSpeed;
        Vector3 arcPos = Laser.position;
        Vector3 footPos = head.position;
        footPos.y -= head.localPosition.y;
        
        
        foreach(GameObject p in arcPoints)
        {
            GameObject.Destroy(p);
        }
        arcPoints.Clear();
        /*
        while(distance < 10)
        {
            Vector3 delta_p = arcVelocity * .01f;

            //perform a raycast to determine if our arc hit anything
            RaycastHit[] hits = Physics.RaycastAll(arcPos, arcVelocity.normalized, delta_p.magnitude);
            bool arcHit = false;
            Vector3 arcHitPoint = Vector3.zero;
            for(int i = 0; i < hits.Length; i++)
            {
                RaycastHit hit = hits[i];
                GameObject go2 = GameObject.Instantiate<GameObject>(arcPointPrefab, hit.point, Quaternion.identity);
                go2.transform.forward = arcVelocity.normalized;
                go2.transform.localScale = new Vector3(1, 1, .01f);
                arcPoints.Add(go2);
                arcHit = true;
                arcHitPoint = hit.point;
                break;

            }
            if (arcHit)
            {
                //teleporter
                Vector3 targetPoint = arcHitPoint;

                Vector3 offset = targetPoint - footPos;
                bool trigger = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, myHand);
                if (trigger)
                {
                    trackingSpace.Translate(offset, Space.World);
                }
                
                break;
            }
            arcPos += delta_p;
            arcVelocity += new Vector3(0, -9.8f, 0) * .01f;
            distance += delta_p.magnitude;
           
            GameObject go = GameObject.Instantiate<GameObject>(arcPointPrefab, arcPos, Quaternion.identity);
            go.transform.forward = arcVelocity.normalized;
            go.transform.localScale = new Vector3(1, 1, delta_p.magnitude);
            arcPoints.Add(go);
        }
        */

        //we'll use this down below
        float triggerValue = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, myHand);
        float handTriggerValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, myHand);


        if (handTriggerValue < .50f && grabbedObject != null)
        {
            
            if (grabbedObject.endGrab(this))
            {
                grabbedObject = null;
                this.GetComponent<MeshRenderer>().enabled = true;
            }

        }

        Ray r = new Ray(Laser.position, Laser.forward);
        RaycastHit[] hits = Physics.RaycastAll(r,100.0f);

        Laser.localScale = new Vector3(0, 0, 0);
        for (int i = 0; i < hits.Length; i++)
        {
            Laser.localScale = new Vector3(1, 1, hits[i].distance);
            RaycastHit hit = hits[i];
            Rigidbody rb = hit.rigidbody;
            if(rb != null)
            {
               
                Wim w = rb.GetComponent<Wim>();
                if (w != null)
                {
                    if (triggerValue > .9f && canDoWimTeleport)
                    {
                        canDoWimTeleport = false;
                        Vector3 hitPoint = hit.point;
                        Vector3 hitPointLocal = rb.transform.worldToLocalMatrix.MultiplyPoint(hitPoint);
                        Camera c = w.c;
                        Vector3 temp = new Vector3(-hitPointLocal.x + .5f, -hitPointLocal.z + .5f, 0);
                        Ray camRay = c.ViewportPointToRay(temp);
                        RaycastHit[] hits2;
                        hits2 = Physics.RaycastAll(camRay);
                        
                        for(int j = 0; j < hits2.Length;  j++)
                        {
                            
                            doTeleport(hits2[j].point);
                            break;
                        }
                    }


                    break;
                }
                
            }
            else
            {
                /*
                //teleporter
                Vector3 targetPoint = hit.point;
                Vector3 footPos = head.position;
                footPos.y -= head.localPosition.y;
                Vector3 offset = targetPoint - footPos;

                bool trigger = OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, myHand);
                if (trigger)
                {
                    trackingSpace.Translate(offset, Space.World);
                }
                break;
                */

            }
            

        }
        

        Vector2 joystickInput = OVRInput.Get(OVRInput.Axis2D.PrimaryThumbstick, myHand);
        float up = joystickInput.y;
        float right = joystickInput.x;

        Vector3 headForwardVector = head.forward;
        headForwardVector.y = 0;
        headForwardVector.Normalize();
        /*Vector3 headRightVector = head.right;
        headRightVector.y = 0;
        headRightVector.Normalize();
        */

        Vector3 direction = headForwardVector * up;// + headRightVector * right;

        trackingSpace.transform.Translate(direction*speed*Time.deltaTime, Space.World);

        float rightMag = Mathf.Abs(right);

        if(rightMag > .9f && canSnapRotate)
        {
            trackingSpace.transform.RotateAround(footPos, Vector3.up, snapRotateDelta * Mathf.Sign(right));
            //snap rotate
            canSnapRotate = false;
        }

        if(rightMag < .6f)
        {
            canSnapRotate = true;
        }
        if(triggerValue < .8f)
        {
            canDoWimTeleport = true;
        }


    }

    private void FixedUpdate()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        Rigidbody otherRb = other.attachedRigidbody;
        if(otherRb == null)
        {
            return;
        }
        Grabbable gr = otherRb.GetComponent<Grabbable>();
        if(gr == null)
        {
            return;
        }
        float triggerValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, myHand);
        if(triggerValue > .55f && grabbedObject == null)
        {
            attachPoint.position = otherRb.position;
            attachPoint.rotation = otherRb.rotation;
            if (gr.startGrab(this, attachPoint))
            {
                grabbedObject = gr;
                this.GetComponent<MeshRenderer>().enabled = false;
            }
           
        }
         
        
    }
}
