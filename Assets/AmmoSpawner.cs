﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoSpawner : MonoBehaviour
{
    public SlingshotAmmunition ammoPrefab;
    private bool _spawnAmmo = false;
    public bool spawnAmmo {
        get { return _spawnAmmo; }
        set {
            _spawnAmmo = value;
        }
    }
    public SlingshotAmmunition currentSpawn;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(spawnIfTaken());
    }
    IEnumerator spawnIfTaken()
    {
        while (true)
        {
            if (!spawnAmmo)
            {
                yield return null;
            }
            else
            {
                if(currentSpawn == null)
                {
                    currentSpawn = GameObject.Instantiate<SlingshotAmmunition>(ammoPrefab,this.transform.position,Quaternion.identity);
                    
                }
                else
                {
                    if (currentSpawn.grabbed)
                    {
                        currentSpawn = null;
                    }
                }
            }

            yield return null;

        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
